package com.lfalero.bootcamp.simulator.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "quota")
public class QuotaModel extends BaseModel{

    private Integer min;
    private Integer max;
}
