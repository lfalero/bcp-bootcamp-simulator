package com.lfalero.bootcamp.simulator.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "card")
public class CardModel extends BaseModel{

    private String value;
}
