package com.lfalero.bootcamp.simulator.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "audit")
public class AuditModel extends BaseModel{

    private String dni;
    private String card;
    private String money;
    private BigDecimal amount;
    private Integer numberQuota;
    private String tea;
    private Integer payDay;
    private String firstQuota;
    private BigDecimal quota;
}

