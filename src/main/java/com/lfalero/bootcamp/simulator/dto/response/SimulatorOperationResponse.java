package com.lfalero.bootcamp.simulator.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class SimulatorOperationResponse {

    private BigDecimal cuota;
    private String moneda;
    private String primeraCuota;
    private String estado;
}
