package com.lfalero.bootcamp.simulator.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class DetailInformationResponse {

    private List<String> tarjetas;
    private List<Integer> cuotas;
    private List<String> tea;
    private List<Integer> diaPagos;
}
