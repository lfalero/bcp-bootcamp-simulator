package com.lfalero.bootcamp.simulator.repository;

import com.lfalero.bootcamp.simulator.model.TeaModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface TeaRepository extends CrudRepository<TeaModel, Long> {

    List<TeaModel> findAll();
    TeaModel findByValue(BigDecimal value);

}
