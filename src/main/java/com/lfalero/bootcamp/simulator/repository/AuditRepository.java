package com.lfalero.bootcamp.simulator.repository;

import com.lfalero.bootcamp.simulator.model.AuditModel;
import com.lfalero.bootcamp.simulator.model.CardModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuditRepository extends CrudRepository<AuditModel, Long> {

    List<AuditModel> findAll();
    AuditModel save(AuditModel auditModel);
}
