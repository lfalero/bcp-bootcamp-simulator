package com.lfalero.bootcamp.simulator.repository;

import com.lfalero.bootcamp.simulator.model.PayDayModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PayDayRepository extends CrudRepository<PayDayModel, Long> {

    List<PayDayModel> findAll();
}
