package com.lfalero.bootcamp.simulator.repository;

import com.lfalero.bootcamp.simulator.model.CardModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardRepository extends CrudRepository<CardModel, Long> {

    List<CardModel> findAll();
    CardModel findByValue(String value);
}
